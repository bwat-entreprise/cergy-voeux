<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta property="og:title" content="Voeux Cergy 2019" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="La ville de cergy vous souhaite une heureuse année 2019 !" />
    <meta property="og:url" content="https://voeux.cergy.fr/" />
    <meta property="og:image" content="https://voeux.cergy.fr/img/partage.png" />
    <title>Voeux 2019</title>
    <link rel="stylesheet" href="./css/styles.css">
    <link rel="stylesheet" href="./css/animate.css">
</head>

<body>
<header id="header">
    <img src="./img/cergy.png" class="cergy" alt="cergy">
    <img src="./img/logo.png" class="logo wow zoomIn" alt="image">

    <div class="hide wow bounceInUp blue" data-wow-delay="1000ms">
        <p><strong>Jean-Paul Jeandon, le conseil municipal</strong><br/> et <strong>les agents de la ville</strong> vous souhaitent une</p>
        <p><strong class="red">BONNE ANNÉE</strong> <strong>2019</strong></p>
    </div>
</header>

<div class="button">
    <img src="./img/play-mobile.svg" class="play play-mobile" alt="play">
    <img src="./img/play.svg" class="play play-desktop" alt="play">
    <p>Lire la vidéo</p>
</div>

<div class="video">
    <img src="./img/close.svg" class="close" alt="close">
    <div id="video-container">
        <div class="special">
        <div id="video-placeholder"></div>
        </div>
    </div>
</div>
<div class="sidebar wow bounceInRight" data-wow-delay="3000ms">
    <div class="box">
        <p>Envoyez la vidéo</p>
        <span>
        <a href="http://voeux.ville-cergy.fr/1" target="_blank" title="Voeux 2019 Cergy">
            <img src="./img/mail.svg" class="desktop" alt="icone">
            Email
            <img src="./img/mail-mobile.svg" class="mobile" alt="icone">
        </a>
        </span>
        <span>
            <a href="whatsapp://send?text=<?=urlencode('https://www.youtube.com/watch?v=0NWKlVx9VR0&feature=youtu.be');?>">
                <img src="./img/whatsapp.svg" class="desktop" alt="icone">
                Whatsapp
                <img src="./img/whatsapp-mobile.svg" class="mobile" alt="icone">
            </a>
        </span>
        <span>
            <a href="fb-messenger://share/?link=<?=urlencode('https://www.youtube.com/watch?v=0NWKlVx9VR0&feature=youtu.be');?>">
                <img src="./img/messenger.svg" class="desktop" alt="icone">
                Messenger
                <img src="./img/messenger-mobile.svg" class="mobile" alt="icone">
            </a>
        </span>
        <p>&nbsp;</p>
        <p>Partagez</p>
        <span>
            <a href="https://facebook.com/sharer/sharer.php?u=https://voeux.cergy.fr" rel="nofollow" target="_blank">
                <img src="./img/facebook.svg" class="desktop" alt="icone partage facebook">
                Facebook
                <img src="./img/facebook-mobile.svg" class="mobile" alt="icone">
            </a>
        </span>
        <span>
            <a href="https://twitter.com/share?url=https://voeux.cergy.fr" rel="nofollow" target="_blank">
                <img src="./img/twitter.svg" class="desktop" alt="icone partage twitter">
                Twitter
                <img src="./img/twitter-mobile.svg" class="mobile" alt="icone">
            </a>
        </span>
    </div>
</div>
<footer>
    <img src="./img/cergy-footer.png" alt="logo footer">
    <a href="http://cergy.fr" target="_blank">
        cergy.fr
    </a>
    <span class="social">
        <a href="https://www.facebook.com/villedecergy" target="_blank"><img src="./img/facebook-cergy.svg" alt="partage facebook"></a>
        <a href="https://twitter.com/villedecergy" target="_blank"><img src="./img/twitter-cergy.svg" alt="partage twitter"></a>
        <a href="https://www.instagram.com/jeunesacergy/" target="_blank"><img src="./img/instagram-cergy.svg" alt="partage instagram"></a>
    </span>
</footer>
</body>
<script src="js/wow.min.js"></script>
<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script src="https://www.youtube.com/iframe_api"></script>
<script>
    new WOW().init();
    var delayInMilliseconds = 3000; //1 second

    setTimeout(function () {
        var element = document.getElementById("header");
        element.classList.add("next");
    }, delayInMilliseconds);


    /*youtube*/
    var player;

    function onYouTubeIframeAPIReady() {
        player = new YT.Player('video-placeholder', {
            width: '100%',
            height: 550,
            videoId: '0NWKlVx9VR0',
            playerVars: {
                controls:0,
                disablekb:1,
                rel:0,
                showinfo:0
            },
            events: {
                onReady: initialize
            }
        });
    }
    function initialize(){
    }

    $(document).ready(function () {
        /*if mobile youtube full screen*/
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        }
        $('.play').click(function () {
            $('.video').show();
            player.playVideo();
        });
        $('.close').click(function () {
            $('.video').hide();
        });
        $('.sidebar').click(function () {
            $(this).toggleClass('active');
        });
    });
    /*youtube*/

</script>


</html>
